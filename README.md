Hi,

Thank you for letting me join your recruitment process.
Shopping cart task was nice!

A few things you need to know:
- I have decided to pick gradle as a build tool because sbt is super slow
- I did a whole task with Scala & ScalaTest, but if necessary I can do it with Java/Groovy/Kotlin
and any other testing framework (JUnit, TestNG, Spock)
- Since I am a big fan of type safety, I do not pass strings instead
I leverage typesafety (that's why Checkout API accepts Items)
- I am a big fan of both OOP and FP but I prefer to use OOP in big picture and I tend to limit FP usage only to methods.
- As you can see, solution is very extensible. You can easily add any type of Item and discount without being forced
to change anything in Checkout class.
- Everything done in TDD style, I love uncle Bob "cleancode" and could definitely
think of some further refactoring, but I was running out of time for this exercise.

It was fun, thanks for this opportunity, and please, even if you do not want to proceed further with this recruitment,
do not forget to send me the feedback! :)
