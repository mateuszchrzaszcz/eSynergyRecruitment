package uk.co.esynergy.recruitment

trait Item {
  def price(): Double
  def offer(): OfferType
}
