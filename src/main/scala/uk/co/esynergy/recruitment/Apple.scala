package uk.co.esynergy.recruitment

class Apple extends Item {
  override def price(): Double = 0.60

  override def offer(): OfferType = OneForOne
}
