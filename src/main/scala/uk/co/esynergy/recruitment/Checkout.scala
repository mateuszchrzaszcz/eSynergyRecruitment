package uk.co.esynergy.recruitment

class Checkout {
  def calculatePrice(items: Item*): Double = {
    val itemsByOffer = groupByOfferType(items)
    val totalPrice = sumPriceOfEachItem(items)
    val discountedItemsInEachCategory = findDiscountedItems(itemsByOffer)
    val totalDiscount = sumValueOfDiscountedItems(discountedItemsInEachCategory)

    totalPrice - totalDiscount
  }

  private def groupByOfferType(items: Seq[Item]) = {
    items.groupBy(_.offer())
  }

  private def sumPriceOfEachItem(items: Seq[Item]) = {
    items.map(_.price()).sum
  }

  private def findDiscountedItems(itemsByCategory: Map[OfferType, Seq[Item]]) = {
    itemsByCategory.map(groupedItem => (groupedItem._2.head, countNumberOfDiscountedItems(groupedItem)))
  }

  private def countNumberOfDiscountedItems(groupedItem: (OfferType, Seq[Item])) = {
    groupedItem._2.size / groupedItem._2.head.offer().discountedItem
  }

  private def sumValueOfDiscountedItems(discountedItemsInEachCategory: Map[Item, Int]) = {
    discountedItemsInEachCategory.map(countedItems => (countedItems._1, valueOfDiscountOnItemType(countedItems))).values.sum
  }

  private def valueOfDiscountOnItemType(countedItems: (Item, Int)) = {
    countedItems._2 * countedItems._1.price()
  }
}
