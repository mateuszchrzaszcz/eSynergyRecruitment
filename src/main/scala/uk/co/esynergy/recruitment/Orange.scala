package uk.co.esynergy.recruitment

class Orange extends Item {
  override def price(): Double = 0.25

  override def offer(): OfferType = ThreeForTwo
}
