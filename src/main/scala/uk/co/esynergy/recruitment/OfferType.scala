package uk.co.esynergy.recruitment

sealed trait OfferType {
  def discountedItem: Int
}
case object OneForOne extends OfferType {
  override def discountedItem: Int = 2

}
case object ThreeForTwo extends OfferType {
  override def discountedItem: Int = 3
}

