package uk.co.esynergy.recruitment

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{FlatSpec, Matchers}

@RunWith(classOf[JUnitRunner])
class CheckoutTest extends FlatSpec with Matchers with CheckoutTestData {
  "A checkout" should "correctly calculate a price of single item" in {
    val checkout = new Checkout()

    val result = checkout.calculatePrice(new Apple())

    result shouldEqual applePrice
  }

  "A checkout" should "correctly calculate a price of multiple identical items" in {
    val checkout = new Checkout()

    val result = checkout.calculatePrice(new Orange(), new Orange())

    result shouldEqual orangePrice + orangePrice
  }

  "A checkout" should "correctly calculate a price of multiple different items" in {
    val checkout = new Checkout()

    val result = checkout.calculatePrice(new Orange(), new Apple())

    result shouldEqual applePrice + orangePrice
  }

  "A checkout" should "correctly calculate zero if no items were provided" in {
    val checkout = new Checkout()

    val result = checkout.calculatePrice()

    result shouldEqual zero
  }

  "A checkout" should "correctly calculate buy one, get one free offer on apples" in {
    val checkout = new Checkout()

    val result = checkout.calculatePrice(new Apple(), new Apple())

    result shouldEqual applePrice
  }

  "A checkout" should "correctly calculate 3 for the price of 2 on oranges" in {
    val checkout = new Checkout()

    val result = checkout.calculatePrice(new Orange(), new Orange(), new Orange())

    result shouldEqual orangePrice + orangePrice
  }

  "A checkout" should "correctly calculate each offer" in {
    val checkout = new Checkout()

    val result = checkout.calculatePrice(new Orange(), new Apple(), new Orange(), new Apple(), new Orange(), new Apple())

    result shouldEqual applePrice + orangePrice + orangePrice + applePrice
  }
}

trait CheckoutTestData {
  val zero = 0
  val applePrice = 0.60
  val orangePrice = 0.25
}
